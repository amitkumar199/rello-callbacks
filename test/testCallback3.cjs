const { error } = require("console");
const callback3 = require("../callback3.cjs");

const list_id = "qwsa221";

const lists_based_on_board_id = callback3(list_id, (error, data)=>{
    if(error){
        console.error(error.message);
    }else{
        console.log("Result Matched");
        console.log(JSON.stringify(data));
    }
});