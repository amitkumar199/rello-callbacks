const { error } = require("console");
const callback1 = require("../callback1.cjs");

const board_id = "mcu453ed";

const testing_board = callback1(board_id, (error, data)=>{
    if(error){
        console.error(error.message);
    }else{
        console.log("result matches");
        console.log(data);
    }
});