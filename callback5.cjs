const { error } = require("console");
const fs = require("fs");
const path = require("path");
const fetching_boarding_information = require("./callback1.cjs");
const all_lists_belonging_boardid = require("./callback2.cjs");
const all_cards_based_on_list_id = require("./callback3.cjs");

const callback_for_space_list_cards = (error, data)=>{
    if(error){
        console.error(error.message);
    }else{
        console.log(data);
        console.log("successfully fetched Space all cards");
    }
};

const callback_for_mind_list_cards = (error, data)=>{
    if(error){
        console.error(error.message);
    }else{
        console.log(data);
        console.log("successfully fetched Mind all cards");
    }
};

const callback_for_thanos_board_lists = (error, data)=>{
    if(error){
        console.error(error.message);
    }else{
        console.log("successfully fetched Thanos Board List");
        console.log(data);
                
        const mind = data.find((data)=>{
            return data.name === "Mind";
        });

        const mind_id = mind.id;

        all_cards_based_on_list_id(mind_id, callback_for_mind_list_cards);

        const space = data.find((data)=>{
            return data.name === "Space";
        });

        const space_id = space.id;

        all_cards_based_on_list_id(space_id, callback_for_space_list_cards);
    }
};

const callback_for_fetching_board_information = (error, data)=>{
    if(error){
        console.error(error.message);
    }else{
        console.log("successfully fetched board");
        console.log(data);
        all_lists_belonging_boardid(data.id, callback_for_thanos_board_lists);        
    }
};

function thanos_boards_all_lists_mind_and_space_all_cards_lists(){

    setTimeout(()=>{
        fetching_boarding_information("mcu453ed", callback_for_fetching_board_information);        
    }, 2 * 1000);
};

module.exports = thanos_boards_all_lists_mind_and_space_all_cards_lists;