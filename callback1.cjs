const { error } = require("console");
const fs = require("fs");
const path = require("path");

function fetching_boarding_information(board_id, callback) {
    if (board_id === undefined) {
        callback(new Error("enter board_id"));
    } else {
        setTimeout(() => {
            fs.readFile("./boards.json", "utf-8", (error, boards_list) => {
                if (error) {
                    console.error(error.message);
                } else {
                    let array_boards_list = JSON.parse(boards_list);

                    const particular_board_information = array_boards_list.find((board)=>{
                        return board.id === board_id;
                    });

                    if(particular_board_information===undefined){
                        callback(new Error("id does not exist"));
                    }else{
                        callback(null, particular_board_information);
                    }
                }
            });
        }, 2 * 1000);
    }
};

module.exports = fetching_boarding_information;