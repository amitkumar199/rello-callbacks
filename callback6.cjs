const { error } = require("console");
const fs = require("fs");
const path = require("path");
const fetching_boarding_information = require("./callback1.cjs");
const all_lists_belonging_boardid = require("./callback2.cjs");
const all_cards_based_on_list_id = require("./callback3.cjs");

const callback_for_thanos_board_lists = (error, thanos_board_list)=>{
    if(error){
        console.error(error.message);
    }else{
        console.log("successfully fetched Thanos Board List");
        console.log(thanos_board_list);
        
        for(let index=0; index<thanos_board_list.length; index++){
            all_cards_based_on_list_id(thanos_board_list[index].id, (error, cards)=>{
                if(error){
                    console.log(error.message, `for${thanos_board_list[index].name}`);
                }else{
                    console.log(`Successfully fetched ${thanos_board_list[index].name}`);
                    console.log(cards);  
                }
            });
        }
    }
};

const callback_for_fetching_board_information = (error, data)=>{
    if(error){
        console.error(error.message);
    }else{
        console.log("successfully fetched board");
        console.log(data);
        all_lists_belonging_boardid(data.id, callback_for_thanos_board_lists);        
    }
};

function thanos_boards_all_cards_lists(){
    setTimeout(()=>{
        fetching_boarding_information("mcu453ed", callback_for_fetching_board_information);        
    }, 2 * 1000);
};

module.exports = thanos_boards_all_cards_lists;