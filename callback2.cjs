const { error } = require("console");
const fs = require("fs");
const path = require("path");

function all_lists_belonging_boardid(board_id, callback) {
    if (board_id === undefined) {
        callback(new Error("enter board_id"));
    } else {
        setTimeout(() => {
            fs.readFile("./lists.json", "utf-8", (error, lists_list) => {
                if (error) {
                    console.error(error.message);
                } else {
                    let array_lists_list = JSON.parse(lists_list);

                    const particular_list_information = Object.keys(array_lists_list).find((list)=>{
                        return  list=== board_id;
                    });
                    
                    if(particular_list_information===undefined){
                        callback(new Error("id does not exist"));
                    }else{
                        callback(null, array_lists_list[particular_list_information]);
                    }
                }
            });
        }, 2 * 1000);
    }
};

module.exports = all_lists_belonging_boardid;