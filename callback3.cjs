const { error } = require("console");
const fs = require("fs");
const path = require("path");

function all_cards_based_on_list_id(list_id, callback) {
    if (list_id === undefined) {
        callback(new Error("enter list_id"));
    } else {
        setTimeout(() => {
            fs.readFile("./cards.json", "utf-8", (error, cards_list) => {
                if (error) {
                    console.error(error.message);
                } else {
                    let card_array = JSON.parse(cards_list);
                    
                    const particular_card_information = Object.keys(card_array).find((card)=>{
                        return  list_id === card;
                    });
                    
                    if(particular_card_information===undefined){
                        callback(new Error("id does not exist"));
                    }else{
                        callback(null, card_array[particular_card_information]);
                    }
                }
            });
        }, 2 * 1000);
    }
};

module.exports = all_cards_based_on_list_id;