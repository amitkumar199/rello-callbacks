const { error } = require("console");
const callback2 = require("../callback2.cjs");

const board_id = "mcu453ed";

const lists_based_on_board_id = callback2(board_id, (error, data)=>{
    if(error){
        console.error(error.message);
    }else{
        console.log("Result Matched");
        console.log(JSON.stringify(data));
    }
});